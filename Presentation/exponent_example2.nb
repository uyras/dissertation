(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     32347,        784]
NotebookOptionsPosition[     30869,        728]
NotebookOutlinePosition[     31214,        743]
CellTagsIndexPosition[     31171,        740]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"en", "=", 
  RowBox[{
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"a", "*", "b"}], "+", 
      RowBox[{"a", "*", "c"}], "+", 
      RowBox[{"a", "*", "d"}], "+", 
      RowBox[{"b", "*", "c"}], "+", 
      RowBox[{"b", "*", "d"}], "+", 
      RowBox[{"c", "*", "d"}]}], ",", 
     RowBox[{"{", 
      RowBox[{"a", ",", 
       RowBox[{"-", "1"}], ",", "1", ",", "2"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"b", ",", 
       RowBox[{"-", "1"}], ",", "1", ",", "2"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"c", ",", 
       RowBox[{"-", "1"}], ",", "1", ",", "2"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"d", ",", 
       RowBox[{"-", "1"}], ",", "1", ",", "2"}], "}"}]}], "]"}], "//", 
   "Flatten"}]}]], "Input",
 CellChangeTimes->{{3.7064251963727026`*^9, 3.7064252474840546`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"6", ",", "0", ",", "0", ",", 
   RowBox[{"-", "2"}], ",", "0", ",", 
   RowBox[{"-", "2"}], ",", 
   RowBox[{"-", "2"}], ",", "0", ",", "0", ",", 
   RowBox[{"-", "2"}], ",", 
   RowBox[{"-", "2"}], ",", "0", ",", 
   RowBox[{"-", "2"}], ",", "0", ",", "0", ",", "6"}], "}"}]], "Output",
 CellChangeTimes->{{3.706425207723318*^9, 3.7064252491116624`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"z", "=", 
  RowBox[{"Total", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"Exp", "[", 
      RowBox[{
       RowBox[{"-", "#"}], "/", "T"}], "]"}], "&"}], "/@", "en"}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.706425241907844*^9, 3.7064253006693096`*^9}, 
   3.706426129540074*^9}],

Cell[BoxData[
 RowBox[{"8", "+", 
  RowBox[{"2", " ", 
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{
     RowBox[{"-", "6"}], "/", "T"}]]}], "+", 
  RowBox[{"6", " ", 
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{"2", "/", "T"}]]}]}]], "Output",
 CellChangeTimes->{3.7064253012369843`*^9, 3.706426138564956*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"z1", "=", 
  RowBox[{"Total", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"#", "*", 
      RowBox[{"Exp", "[", 
       RowBox[{
        RowBox[{"-", "#"}], "/", "T"}], "]"}]}], "&"}], "/@", "en"}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.7064253216816835`*^9, 3.706425334667637*^9}, 
   3.706426134149413*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"12", " ", 
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{
     RowBox[{"-", "6"}], "/", "T"}]]}], "-", 
  RowBox[{"12", " ", 
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{"2", "/", "T"}]]}]}]], "Output",
 CellChangeTimes->{3.7064253362295475`*^9, 3.7064261399772067`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s", "=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"Log", "[", "z", "]"}], "+", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"z1", "/", "z"}], ")"}], "/", "T"}], ")"}]}], ")"}], "/", 
   "4"}]}]], "Input",
 CellChangeTimes->{{3.7064253465534062`*^9, 3.706425384606183*^9}, {
  3.706425470692461*^9, 3.7064254764940987`*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "4"], " ", 
  RowBox[{"(", 
   RowBox[{
    FractionBox[
     RowBox[{
      RowBox[{"12", " ", 
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{
         RowBox[{"-", "6"}], "/", "T"}]]}], "-", 
      RowBox[{"12", " ", 
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{"2", "/", "T"}]]}]}], 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"8", "+", 
        RowBox[{"2", " ", 
         SuperscriptBox["\[ExponentialE]", 
          RowBox[{
           RowBox[{"-", "6"}], "/", "T"}]]}], "+", 
        RowBox[{"6", " ", 
         SuperscriptBox["\[ExponentialE]", 
          RowBox[{"2", "/", "T"}]]}]}], ")"}], " ", "T"}]], "+", 
    RowBox[{"Log", "[", 
     RowBox[{"8", "+", 
      RowBox[{"2", " ", 
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{
         RowBox[{"-", "6"}], "/", "T"}]]}], "+", 
      RowBox[{"6", " ", 
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{"2", "/", "T"}]]}]}], "]"}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.706425387762517*^9, 3.7064254772238436`*^9, 
  3.7064261413671956`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"Limit", "[", 
  RowBox[{
   RowBox[{"Log", "[", "z", "]"}], ",", 
   RowBox[{"T", "\[Rule]", "0"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"Limit", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"Log", "[", "z", "]"}], "-", 
    RowBox[{"2", "/", "T"}]}], ",", 
   RowBox[{"T", "\[Rule]", "0"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"Limit", "[", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{"z1", "/", "z"}], ")"}], ",", 
   RowBox[{"T", "\[Rule]", "0"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"Limit", "[", 
  RowBox[{"z1", ",", 
   RowBox[{"T", "\[Rule]", "0"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"Limit", "[", 
  RowBox[{"z", ",", 
   RowBox[{"T", "\[Rule]", "0"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.7064254100310383`*^9, 3.7064254128070316`*^9}, 
   3.7064264688957186`*^9, {3.7064266321480083`*^9, 3.706426664239008*^9}, {
   3.706426765272398*^9, 3.7064267686751585`*^9}, {3.7064268264571743`*^9, 
   3.706426845965515*^9}}],

Cell[BoxData["\[Infinity]"], "Output",
 CellChangeTimes->{
  3.706426633065757*^9, 3.7064266651039567`*^9, 3.706426769292585*^9, {
   3.706426827521352*^9, 3.7064268464321976`*^9}}],

Cell[BoxData[
 RowBox[{"Log", "[", "6", "]"}]], "Output",
 CellChangeTimes->{
  3.706426633065757*^9, 3.7064266651039567`*^9, 3.706426769292585*^9, {
   3.706426827521352*^9, 3.7064268464912357`*^9}}],

Cell[BoxData[
 RowBox[{"-", "2"}]], "Output",
 CellChangeTimes->{
  3.706426633065757*^9, 3.7064266651039567`*^9, 3.706426769292585*^9, {
   3.706426827521352*^9, 3.7064268464932375`*^9}}],

Cell[BoxData[
 RowBox[{"-", "\[Infinity]"}]], "Output",
 CellChangeTimes->{
  3.706426633065757*^9, 3.7064266651039567`*^9, 3.706426769292585*^9, {
   3.706426827521352*^9, 3.7064268464952393`*^9}}],

Cell[BoxData["\[Infinity]"], "Output",
 CellChangeTimes->{
  3.706426633065757*^9, 3.7064266651039567`*^9, 3.706426769292585*^9, {
   3.706426827521352*^9, 3.706426846497242*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"s", "//", "FullSimplify"}]], "Input",
 CellChangeTimes->{{3.7064267130498114`*^9, 3.7064267214342184`*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "4"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", 
     FractionBox[
      RowBox[{"12", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", 
         SuperscriptBox["\[ExponentialE]", 
          RowBox[{"8", "/", "T"}]]}], ")"}]}], 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"2", "+", 
         RowBox[{"8", " ", 
          SuperscriptBox["\[ExponentialE]", 
           RowBox[{"6", "/", "T"}]]}], "+", 
         RowBox[{"6", " ", 
          SuperscriptBox["\[ExponentialE]", 
           RowBox[{"8", "/", "T"}]]}]}], ")"}], " ", "T"}]]}], "+", 
    RowBox[{"Log", "[", 
     RowBox[{"8", "+", 
      RowBox[{"2", " ", 
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{
         RowBox[{"-", "6"}], "/", "T"}]]}], "+", 
      RowBox[{"6", " ", 
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{"2", "/", "T"}]]}]}], "]"}]}], ")"}]}]], "Output",
 CellChangeTimes->{{3.7064267165269346`*^9, 3.7064267227491045`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Limit", "[", 
  RowBox[{"s", ",", 
   RowBox[{"T", "\[Rule]", "0"}], ",", 
   RowBox[{"Analytic", "\[Rule]", "True"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.7064254256678305`*^9, 3.706425448566536*^9}, 
   3.7064254846981974`*^9, 3.7064265030402803`*^9}],

Cell[BoxData[
 FractionBox[
  RowBox[{"Log", "[", "6", "]"}], "4"]], "Output",
 CellChangeTimes->{{3.706425436598992*^9, 3.7064254490887437`*^9}, {
   3.706425480312091*^9, 3.706425485101946*^9}, 3.706426142595074*^9, 
   3.7064265038977165`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{"s", ",", 
   RowBox[{"{", 
    RowBox[{"T", ",", "0.001", ",", "20"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.7064259154962273`*^9, 3.706425947606439*^9}, 
   3.7064261199120474`*^9}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
    1.], LineBox[CompressedData["
1:eJwV0Xk4lXkbB/BjOejYHYwt2Vpk96Ri4r4zvJZUJikiydZQtqJM0SKylCSU
tUQ1V8opKluWpmxRmdAgKpzneSwplZTUYZ73j991X5/re/9x/66vtl/4lkBh
FouVwrz/TwerpgZleg7M4lgmf5F8sDneKrXa/wc4x4YpD3zkQ7P4qcHPXAF0
iVu+cfyFBJuPJ27MGwug3jdhPFmVhMq+uIMcZwEEJX4ob1In4fqNaDmd4wLY
sVBUaqJFQvKGQLvfJwXQpl+Q2LOCBJd0u1u3m+bhwTPJW/6WJASfFrKptGKh
pJuX75vtJChmlGq4O7Awr1SPc8aThIYLbj+m3Vj4IfWx3BovJi++Vm0WysKm
tUMbE32YvNqJKLvMQk/vJ6tEAklQoDNWXBUVwhx3p6GUSBLqUId7/rkQVm66
GDaWTELQ/zo+mQ4Iocf5pSUuqSTIu0T90zkqhAWOI695p5l8e0uatJAwVt1V
PxR6lsnDQiRSCGGEUUtedyaT51cIjuUIY63Vw3sOl0iQ/Wo7Fuonggf33RQf
KyfhKl1jrx4hgteNxM8b3SVhba9pSVucCLZnm7tF3iPBt1rTRzdXBI8qWqZM
V5Jw5/Bcd1+nCJ697W7If0CC60J542/Woug+ahC2v5mEDAnti6oqbDwofKN3
Ty8Jy75f/NKylI25peGvD/eRUDsusyWKYKMLmp4400/CSPtPqc5NbHzY5VR2
c4AE87S++IRENgZozA71vyWhS/5c2NRnNs5PpY19GyWBq75g3/xMDIN4Zo3O
35h+5eLbzAfEcN+X++NmsySEsNkbisbEsLI2ka3ynYSHUxzXIyLiGLRii8rw
HAlhTco7zCzFcZWgfDBonoT2UOOwwqviOBPr4W/LpiD+4c4L0Uck0PuxV7md
AgW8+0Mq/GQJPM3joTSXgv5S/3zXCxJ4x0b7Sw9j0+zgIoNyCex/3Pt0txIF
b4KjS4dpCdQSE90bqUKBFTetYeOWRSguwYsN1qTgc2AdvVSfg6H/RB5u1qfg
zL6Q3bcsOMg6Fbs6eCUFyw6oDJrbclDPJ3OrlAEFO45HvQAvDrInHGtdDSn4
O8+wzjONg3d0zX/rMqbgXGdBRtonDmZSr1k1BAXGlnHrZmokMblX6KyaNQXB
Ujbnm5yl8azu8fqtmyiYFNSc9vSQxjPbnWuHGUdMWSR+CJTGd/SIadhmCg52
GcWonJBGf1G7s4muFCTkLPbZVyWNWbN8Dd4WCi7pCfQV9WRwTcxlzdFtFPSs
q/vbXyCD8UnftAQ+FBStLowR8pDDmLJZi/5QClSrIyI9fOTQJ0Jsbn0YBect
7UJuB8hhbsH1P0oZJ6x757UzUg4lS1QdDodTsMfWEmpS5TB9gxRXOZL5z6aX
7Mh6OeS5xayxiaKgLlA6c0hHHhOjc9TcDlPQmxVX1vheHg/+mit+JZGCApXe
6q/T8ngoz8F7hrFvgWmT0Zw8tjYu3u90ioKJEv6rfDEFlE+2HvnI+GeFk0TM
EgWcCNAcWJdMgdYLJX/T3xWwcfJ7wJNUCkKkeSpX7ikgR38+ujKd2U98czL+
CBd56ktPT+ZQYLFw6TnGc1G9NYtcm0tBWMwu1YVkLo58EBZNZDwcMsQ7coGL
1kRLtkYeBS2bR/qjyrnou9dR0imfggxV2mTPKBf9BPYRFwuZ/sveD2xwU8S8
a80lksUUuPb8JJQMlPDydCffu5TpyzVuvKVeGYdy3P/kVFPQqP8gxnG/CoYc
MigtbGLuy/7zxnNNNTx1t23bkk4K2KZ5bw/w1HFc58fnrF4KrJfHTI3tWoxG
K+wC0t5SsN6lWjTuiybWO8FaKZqCX0df/RnhroXLoqyuWk5S0PFJ9dFVQ210
XLJy9dlPFNhyOWl/zGhj7De97NkZ5j5+n51ipw4GL3mlYz1HgV27x7HNeboo
Txw5/kxAwe25HE7abj2Ml83b+4VFQ0V/VXyF5VL0rpjz44jSkN+ZFLdHZBkG
l2z1viZGg3rU6kDRN8vwGGfQxGIRDQp+1wpL7i3H5YIdHnGSNEwFNXt0xa7A
/YNNGuHSNASoNbHG3fXRmpL12ilLQyN5wCBdayWKmktliMrTkDHLv75mdCVG
3yiOOKlAw1ytuWxbgwGKxWRVCivS4JRlPjeaZIiyTSc1NirRYKoh3zW12wiL
DiZk5yjTIKgtMkwyMkaDyzvDqn6hYX3Jx16tGWNUcqryq1ehQcNqSUTxMxNM
yi9+16zK3HfgWHP/BlP8IZvNuaXG5MU+TttqTHHhcVBzljoNojOFM916Zlg0
lskP1qBhV7eFv2qKGXIC1HX1FtMgtsi6tWvGDO2KHiUPM9ZqL5h08zLHxzKh
J5M0aZi57W31b4M5Ynrja60lNCi/P3306UoCbSaFM+oZl97nJqSmExi7uMHP
QYuGn8Jjni4ZBCqL36h2YrzJtc5EJpPAk8fmpV0Yf54IGMi4QOAx/nilK2Mr
rUrz3EICvw23Cu1g3JHqMfzXTQJfNncc2cd4clehTXMrgWqhDsIZjG3KIhWT
nhBoOz5in8n43Jz9hGMHgXzhqeRsxquyP2Q/fU6gcYendB7juHZ43/2SQPue
o7IljGUsRvJH+ATq3jL5fo+xb3xl5FWKwMjpCaKKcUVnqkPQKIHaj1TDahhv
CyGmxycITCzrGqpnfOlygvOnT8x+Y2VjC+OPkx5ad6cJ9LHnfm1jbGtl9DVq
hsCUh6RBB2O65+WV2VkCV7iMZncyXqtTeqh2jkCLKpX2F4xTw49ujP1J4J2n
jYJuxoN1W3Rt5gmsrRk0/ZexCWf594UFAnfeDPfvY/wfqmUE0w==
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0.618},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None},
  PlotRange->{{0.001, 20}, {0.6216617506033001, 0.6915094320405434}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{
  3.7064259486232667`*^9, {3.7064261202527914`*^9, 3.706426147476424*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"c", "=", 
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"z1", "/", "z"}], ",", "T"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.706426068788184*^9, 3.706426089256235*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", 
   FractionBox[
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"12", " ", 
        SuperscriptBox["\[ExponentialE]", 
         RowBox[{
          RowBox[{"-", "6"}], "/", "T"}]]}], "-", 
       RowBox[{"12", " ", 
        SuperscriptBox["\[ExponentialE]", 
         RowBox[{"2", "/", "T"}]]}]}], ")"}], " ", 
     RowBox[{"(", 
      RowBox[{
       FractionBox[
        RowBox[{"12", " ", 
         SuperscriptBox["\[ExponentialE]", 
          RowBox[{
           RowBox[{"-", "6"}], "/", "T"}]]}], 
        SuperscriptBox["T", "2"]], "-", 
       FractionBox[
        RowBox[{"12", " ", 
         SuperscriptBox["\[ExponentialE]", 
          RowBox[{"2", "/", "T"}]]}], 
        SuperscriptBox["T", "2"]]}], ")"}]}], 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"8", "+", 
       RowBox[{"2", " ", 
        SuperscriptBox["\[ExponentialE]", 
         RowBox[{
          RowBox[{"-", "6"}], "/", "T"}]]}], "+", 
       RowBox[{"6", " ", 
        SuperscriptBox["\[ExponentialE]", 
         RowBox[{"2", "/", "T"}]]}]}], ")"}], "2"]]}], "+", 
  FractionBox[
   RowBox[{
    FractionBox[
     RowBox[{"72", " ", 
      SuperscriptBox["\[ExponentialE]", 
       RowBox[{
        RowBox[{"-", "6"}], "/", "T"}]]}], 
     SuperscriptBox["T", "2"]], "+", 
    FractionBox[
     RowBox[{"24", " ", 
      SuperscriptBox["\[ExponentialE]", 
       RowBox[{"2", "/", "T"}]]}], 
     SuperscriptBox["T", "2"]]}], 
   RowBox[{"8", "+", 
    RowBox[{"2", " ", 
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{
       RowBox[{"-", "6"}], "/", "T"}]]}], "+", 
    RowBox[{"6", " ", 
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{"2", "/", "T"}]]}]}]]}]], "Output",
 CellChangeTimes->{3.706426089824188*^9, 3.706426151836968*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"c", ",", "s"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"T", ",", "0.001", ",", "2"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.70642609346926*^9, 3.7064261151261816`*^9}, 
   3.7064261561142807`*^9, {3.7064261880213785`*^9, 3.7064261967809453`*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
    1.], LineBox[CompressedData["
1:eJxF1nk8VF0YB3Ah2SpLEakQ2coSSjL3oRSVJEmk7Hsl2VIpQ4WKkn03IYms
SV6ioQiVXUSYi+yhUtZ4zwxN/uDz/fye85xl7j1GxPKivg0jAwODKPpF/cus
3Eq6YqkHDMs/84uispMXTtN91ebl2ZdqlnRfkmcaUdA+T/fvu81RU6nOdOdf
l1D7JOVKt4vz1e78Z+50y1t99A2V81x2TsnYSeFtLnlX6fkzbddqPeXrdDvu
rTwvV+j9t56wLpKT1X+Tz7JJBFeuAuPfpr70+sY7Zhk2pJt/8xJ5JrY/zfgt
ev7AK09Xc6sf3WO/TEjPrf2XHUzQubjyh2hqAD1ntzQKXyF1j26HjhUDzo6B
dFcZZKhQMoLo4yVqDe4e+3af7jdbFy+Y8Qcvm1jiQmni4y8Mpo8XjU97XWv0
8N/6ja/b+838NZHgw6fPjcWE0C3ftK3ol2oovZ7yYM4ys+Ofg3XqOWy8wugG
tsf5QpvC6ePHKq6cbS4Jp+cJvrosgaYRdB+Frdn7F/95fm7q1Fxi5L/Pp/AD
w3P1KLpN3B89dcT/2rmEfaeHvqhvND0vGjs891k05t/nmbEl5eGbGHq9gP2k
ziHr2L8mVIlV/2JYGUe3Jx6fUPj4nyUTXLScD8bT+7We1pqQGPhnP36h6G7/
BLp3NU9oREom0uf7GlwxrFv9zzXbPh5iUiYt25wgenv3ZrPbJPr4q72PfhS1
0POSRg3Od3zbHtEtTfKIdfGgm+C7QLlYW/mI3v/zmSOa0vxJ9Fy++MUGPzu6
SwIEhL/hL//llMt3ywisyfRc5dNkeLRRMr1fsJKZ46+0ZPr6BkOqseMz/+rV
vyvyZh5KofeLPJYwwBqTQq+/v0lMWOL7v9xv5KnRAe3H9PE3/pN7aJX4mF7v
4f+i2uf3Pxu+/pAxq/+Ebt0g/b4N6f980KRNaPeKNLoxKdOTJ43+WXmqN8g1
+6/1CNsrHCofsjyl52Kh4wvZZ/9ZyMJ9d23+P6+Tm7s4ypFOH8/5h5jGbpVO
z5nfs+CSRfS8ZD4qUECLO4Oe/7Tl0bex/+dhpai7N19n0M+jh3Hzm0d8z+j9
2+uT515feEbPGxOklLre/nPN+ezz8xszl21PKFdVfizo+tduhCLW4k6Vmr/2
JeR+Uuc7JZK1bD9CWkqlrrvnXz8kkFx0/EPr/jqSEKXe+Dp3W/aSiSRC8Bqj
6brrf/2EEPClU36sednqOQT1llwlFr6cJQsXENReTeqFayybVEpQSd59QezC
snPeEpTuXr3zPGrZE+8J8pdKHu97+zdvJGw3YihvGF82pY0gAfu7zDfmLtmL
QhDd5jc7fnDZhV8Jm1dX83m7LNt6lCA4yaG4JmHZN34Q+Dp0j8VXL/v3FIGn
/OG57b+W3bVAWPO02b9YOG/JtUwYezB/ymGdZWuxYSyXT5M/X172ai6M0TT+
i33ykok667AFTcr0VO2y7wtgszJb1/vPLll9pTD2m8dWgW/b8+XzFcN+zKQd
fXx8yeROSWyMMuKgdH3JxPey2PA7Wb83acv5wk6sP+tSkn7zso12Yz3h+aX4
4rIj1LAur6l2Z5l8mvUuaWDtVqpTi4ZLJisfwD4dvs77wHfJwvmHsEYFstzm
rCVzcR7Dajcw6WR+XnJhjj5Ws3jAXm3lC5qrnAyxiv6AW+/ll5zWehpLzc1/
etl4ydpPzLAAL0rtVt/lPN4Sc9TinKxLXzKp1QbT4VER8Gpa7hfsgMl2WmGS
80uWb7+ArU17YNUsVkCzk9UlbMKlOIB4dMmTym5YI2Egc7vHks84X8byWXmb
2hKWHLflGhbRhE3ferdkBn1vzDPBcZPCBM3EVA5fzNghYl/nhpe03Jl8C1NV
Kre7o0Ezsa/eHxNa/Bao7LiUX9G6hy1UC+ThIUtOlHuAUcIOtAYVL9WP1DzE
ys0uze/po1m9d38YliIdL9LPWUg1eRt7JOb3q+pgiDLN6jXiMZgdefIcZkoz
Q3hiPKZ9T/jhsB/Nwn5kEiZtqFMQkU2zOQwnYZwinh372pbqA/QfY2MjyQzj
DP9RTbFafILVF9SJx0rRLHxsKh3L85k7rKVPs/oBjSwsTEfC+efV5fofOZg7
/4nwxOSl+p+TeZhhz42iIx9onkja/gJTyUzvnpqk2XldwktM0PMTc8qmIqqD
9+oWYfP7GKX1DtKsbrWvBOtaLXts3olm4gZ1MkZuM3ZLi6SZq4WlHEtKvh1t
QKZ5Onb6DXbTKbeUYYjmnJYNlZj1ns7eZ9zFVAeInKvCDjKzsRmrLvnUzxqM
Pcb8RO49mplZTOqxUetAz7P5NAsXzDdgH+UK49k6i5fOq7wJy5rtLX+x8hXt
vGSzWrDgirWDFrI0s+5804q5BO9dveYUzVHCzO2YgYndziJvmjl/uX7B+L+X
evE00Ezy/U3BpouHH5XO0Jzjc6MHa/fje+coWkK120uxPuzV8X2jfEdoPs0x
8hVLEHLifuNKc8+1pgGMOBC962LcUr6FMoRZ5FWYbKxYGi/GO4qJaW9KdeUr
pXpP5dUJjIX30PstQLNJf/N3bLDTbeK9Hc0n12v9xGrSSOs9g2m++KxxEstw
/aAq9h/NXLxXfmOB2LRZPU4z9+u905gTm9htL/bXNCsLzWI7E6/VNZvQzN+p
toD9GPr+Iv8m1UTpU6oMkKdkHxeWQctFyuxWgGKNgYPBHM33H8wywdW7XyVi
uMnIZPtyi5XQni288a441UQSbxcLxMxGrHA8SrV6MbMgO8xuaZw0tqC5IK+D
A04fWD14yJ1m19iC1SAYfLNWKp5qytXDXVxw9UVpmUDuUj43yQ3t7TP5bBVU
C3PHCvJCrLhz7NAo1SRVifD1MH844/5nhjJqvaHZCB+cce73qV5H8+FNJzaA
UPEZ+6dqVBNP/DTeCF6USJNoParJG67NCsGXlU26d6yppojbZ2+GOL1Dyg6B
VAevK98uAvPutySNSVQL922VFIUzsa83Hsqn5cxH5baCUL8yo9QXqnMSki3F
wYvj0q8NE7R6mawH2+CL/LNBVuZy6nrkVWskIP6aaN2gDNV6uekO0rBAOlve
BjTzedfJgGll1IuqE1RPvM3X3AGbudfGpV2j2qgpQU4ebuw6/CDqAdUB5dWM
CtBlcts3IJnqtsUnXQqQkDpnb/+eakpTUK4iLLzfdcaom1a/QvipEph+v3RM
+yfVkmlX05Vhs9qgsuTGN9T9rM54vxsSG5oZBx2pjjIVDVYDhqm1v1tvUL3h
imAXAcyFjgy9C6Haef1eFQBhu7K6J0VUu4lsYtMAYuB8eWQt1dN1ptUaQMnd
XeDfQzXF3zh4H5DmM+Ps2N8iW9uKKWoCg+jQg1ObqeYaO7z2AJhrid3U2km1
GqH1xwEQDolxkDhNs1hTvRY8kvDfNfCUas8O3W9HYFiyutz/NdXq6XyiR2Gn
NMcxyRaqo1q0LHShfPsDOweGCmQ+72/eesAu2/CTjY/quBNf5I6DvhwvMV2G
atUb174ehx6FyKgRQ6oNlA5ZngBpxc9igeepDvR5KWUALkobc7f7Ur0u5eSU
ATDtTqy+kEn18L7kx4YgpvZkdpypElnQe6P+aThPGPILFqCa5V6CrgnkYzK8
CnJU530w1j8DmhrZMi6nqTbYwOBsCjYHX56ZzKH6q0bPTQvI0poZDKuk1btT
hC3ht/Zed+UvyERTfaZyS/A78jrIY9U7lF/ln+eyhlS9d6XTZ5GJ8ncSem1h
7DjbkWhXqgOvpSbbwa4TR1r33KHW26WftbeHypN141fzqfnmzVsYHGHgdKvw
H/Yq6vOr1OR4AeTPCGTGCyMT7/Xs0XcCz7Mme7Bd1Hycsx27CKzm3ce9LZDV
T2StDHMGSZsB3xWF1Poyh1UJLuBsK7U26SMy+anWAwlXKLQ7F7uvlzo+/8XU
c1fQdhx/fnNNNRo/cu9eixs4XJzqW2mDTNRtfax2GdKvrNJi56lB4+N+5j3y
gq+jJaf2bKP6o46H3nUQNnezt1dFJt5qlWK8AREH8TuVlsjqXHVXnLzBl7f4
g+9zZPNHhDxlHzDJdNKfM3gPDMLRsy+v34IIYXEraQdkYrzOzLdb0Bja4Wp8
nWrp9RSz26B9VTv85WNk8xm9NG0/UNISbXP9jUwWueMgGwCceMvZ0cgPaL7L
HeU6gaBlEOi08RnVCb3X2wLB990+78PkD9T+3ky2QTCdmU1KG0SmVF728LsP
fVfv9Frv+Yj63S2IcgqGV+sIDp3tyHrcLyu+h8CU/88rnOPIXDMstgdDYefc
07t7mWrR+g9emokNhTSc71m0DLK68en5Q2EQljUxdtIL2Vzh3a3ccLigneJW
u6kOzXc+USQ7CoIjLTZnKSKTGYSeboyG/P7NVUGHkIVr3YzuRMPsreiNR92R
9SoS5e1jwK/sfvn7D8jEmvBGxTg4Fq8W0EeoBwb7TbwhK0hA0dx4tlkbWY1l
TEqDBC6jMwpvTyA7H5g45UOC8L0vO5IdkNusfD2YHkHHZwU5q3DkwJfVJWuS
wI5P4hM+ijyakeKgkQLTJSszGqaQb92dM7ubAndt+rzLGBvQ/uwtRppSICv/
kdSjDchfvFPJ9o9hUl/ourkmcpjFbbPoVCA+4BbrjkXWtPJW4XsKUazzl74c
bgSGV2L+672yQERvx+7ok8hCyV0eLVmQHmk6f9Icueq2zVm5bHi1rcyvzh05
rkGgsC8bcE2/2HISsvq09Pv0HJD24apI+42s115BlsiDkhmxDe5JTcDAftKO
Ve8F9AwfLV092wwMBzOswrRegfW5/yq4mVuAIU1/LOjGKxgYEfu4fg3yPDab
9eIVjI7OdWwSRS74dMRWrASmxp7O7DiMXEt4S2IqBc6fK5V1Y5DNGfvmql7D
rrmSjPuqn4DhBpfM9YIyuMMhG732Wit6n2Yssm5WgMLToTCxne3AsKroyzqJ
Wth1W3ELe/EXIJ+3qNuxvgm2momXyuzuBuIP14cK5E/AZjKD5UxSgHwh3CBP
sx2UeCIOFc4gH0oTqdBrB9PqnQbkBQqYX3j3oe5MO+TvPu9Qz4qDuaBh7gc3
lK/rDp0QwoEo12dlndIOeR/fDsofwIF0/oIQG2MHnFEPDskNx0FYtf4X/8sO
yBKX6M/b1QNEZ0aNyVWd6H1byS+h1gPq1j/tK3g6YWpFn1asRg+QFd+EB27q
BPUu0tObOsictX/md3ZCU4TgBQPLHqBs5WezPdsJ06vWTP4KQt7Zus84pxM0
hidX7PmK+hfWXKvS7QJbo6admcMo11m73sSoC+5W5lqJTPQAQ8K26l6LLmhO
cqpgm+sBcxt//1Y3VG8yGNDO1QtE33P+2jGo/kPHWi+1XqDse7FKvxfVZ5Vv
eh2Kcja/JC6nbpg8X1MpEN0LwlyhfPs9umG9TONFtwTkA5xw/kY3GD7By6Se
ovrLq14m3e+G1kQG27BS5HCBO6SsbmgPxrIchnrBXKJ1Hh/tBtylCFsHfUCR
7OzSUqAAo0L5gJNmH8g/iFmoVaKA6Hh1cPWhPiCT1qYeV6GA5bnPPTcMUG4a
H3IIKNBnNe035NAH9aoHuZiPUmDQYFddaVgfMKzX43tsR4GxXXlmDsN94HxB
/mFyNAXucbc+tRtH83FbZJyLo4Dk6NxPm0nU7481r3wi6v/oQIDlAurnzd+U
mUKBVo7POSY8X4Es+0HtejZ6TiiLK3RVv4J6APfGC28pEHL3aMrOu1/BXoNL
P26EAnI2LmPyD74CVyMzfP9GgQ8QqSIX9hXkLV9l7pugAMsvygeZhK+wgV+N
vwM9h9fM3H6J5X0FZ/dth3v+UMBaKfYgf/tXmGZ7n31kLQ67ugYH56T6odCM
6XGEHA4fLx4ztpXrBxU1K700BRysVxRU1yv1A0kxVKRAEYcQsZvpj6Ef0u4R
scrdOIw7bjp/7GQ/eOI3kwoBh7Sp4+NJROQffesGdXGAO4Vmq/36QTJotXyJ
Hg6fBLfUX76HHPo2/L4+DszYaI5ORD8IZwgf3maIg8UtP5ffGf1gpHA9Uuos
DoLcxb8OtfZD1dZLH30ccchNErHN/9IPrGFSaULncdBWCvi0uQetr+/Jj+cX
cPA4dfLlz9F+mEjUpbQ649AUP+4ZzzgAE0bFZr88cAiUFpuf2DEAAdjxuQBf
HBb3BzFG3RoAlc1FHFsjceD5wbUu9c4AmHfZO6lF4bCNFCaef38AuHhvTulH
46A7H6tdHzUAehrhty7H4pCQn36fNXMAjOrtXyck4qAmXiV4pQXNJ85iYfcE
B72mI9v92weA1HRsjUEaOk+fOkJ49wCQjc0YsKdoPZ2fzHOHBqDeyqyILQOH
jvCvqUN/UP6grIiYhcNVFibF09sGYfCtYOhYPg738/007WUGwUic+WLOCxyS
LNkNPeQHQVi+IfNiAQ41pdxXQlQHIbg3xKT/JQ4Cl4Vf1+gOQuFltmf5RThs
F0+pbzsxCJ7Kdk0WxTioN0n09BsNwnRw1HnOVzjYy8mtZLIahDSWgTnjEhwK
BwhH9l4ehDZWxfJPr3H4EF525pAXGn9F/tlFMg6U/QecTvkMQn13Gx9LGQ6s
JJ2HrvcGgXT8vrtMOQ6njExaMxJRnrYj0OgtDr+rPK2EqlB+tv4/QhX6/GWx
2IiPaP7Ogh+FyDyhTE1cTYNQVRNlplCNg+zZ+/tWdg2C9voHXQI1ONh8TxYZ
+zkIATNdudXvcXAxdDC2nxkEPTV4L/sB3ZvFsg97FtB+01llHiLH3f6PoZVt
CCgrGN4d/YhDs0Bd9+stQ+A845aTWotDz40wflXxIfR5bSD8Rp7oNT6WLz0E
2g8tRffX4cCZ1Veapoxysz+LzciCvOm/RfcOARFvmheox0HS86JsvPoQFHac
NTmDvH/fbPzDI6j//QcHPyPrPXndwnF8CCT7Clp5G3Aw5by92s8Q9R8ObjqC
fPXT2utXLVC/LSfW5SMH7G3Jn7QdAk/1/LN9yBGkmFGn80Mg//M/QZ5GHPIc
xc9YXR4C4cjYaRtkct1waKfXEJBThzYFItcq5bw/5TsEer7k19nIX6LdmRr9
h8Bo9SlKPfLwgupenaAhyDnc6zOOPG3F4FoZgvZnEpPK0YQDS3VFunoU2o93
jp448jrZez1F8UPAym3sq4YsEqonqJw8BPW3KwnHkWWn1+tnp6H1OEnftkJW
O9txRyoLnbfLi9OuyIfLSWXJz5ENIslEZCMJ25lN/w1BwMhC4T1k20AZhahS
tD4xwYNhyG7fJ+x53g6B+Qo+xxhkX8MCUmA1yqM2SyQgBxdfa2OpQ+vlNvNO
RE4Q1uDyaUbnnbbCnZo/u82iPfcZ5aly7LHIRcPvvd27UW7PrRGO/O7Yw5fj
fUOQZli7MQi5Od9w3GF4CEhDgXG+yD0CQhJ942i/qS4V7sgTN3BT019DsGFT
SpQt8kJvakTb7BC0DWtuOInMeeh8rT7DMAgXXD6ogSyYpcDyceUwsMqZissg
S/JOEbQ4hiFgjcBLHuRdnq/cy7hQvr7qzxQ6f81On8y9fMNgzn53rh1Zf5/W
1xcbkV955RUjX+BsNEjfNgykJ7YGHsgeZWRmju3DEPxWfFYPmeiRnX9OYRi0
9RQ9pJDDKIHrdqgNg72K1EwTen4Swq+9DdIYhjaWVcypyE8OO7qNHRwGPYnN
39yRi/K1mrOPo/WwfbZYi/zWYdctrlOof7Xj9zb0/NZuFle6dGYY5D9z25GQ
8QDGsJ12wyD54BKnFDLr2ZITL7yGIYpxwV0EvR88PM+Y+HyHIefau/Pt6H0S
ehfz3MN/GMjj8sYPkeUUPHlVQtB+FiIEZ9D7aLhSsanoyTA4HzZ3z0Hvq3mR
yM2NmWj9FIquMbLDRS5Fr7xhqHqnu4MB2evztxBCCXKVH4M2ev+TM9P0yY3I
gZgyGd0XzywjGUXahmHCbdU5Y+QCfr88n050ni8+5E+g+6XGx4pn/yA6L96y
ewLIEyc3N1b8GQYi6cg1g0ocsIXQ4+8lR6Cq5/IfZ3R/aT/3XbFddgQKW9L0
RtH9pm9/KTdQcQSM2He12CDbNOpy62IjMLgt5soJdB8GprI11J8YgaidbywE
SnH4rOut9+nGCGiv4ceO/4f2J3zvUfvNEZC/OXuiuBCH/u8RP7oCRoCcWz++
FflHWFbYQMgItLXNlk6g+5yzo7NtOhXVGyT1eKL7X91OzWJj3Qhw8YVd1szG
IdV75pL5llEojP9zqTEZ3e/HV76xFhuF6cu1P9ciPxflXucgNQr5eZoSOkno
vqiQLLikOArz4VZpZBJaH4fRrK/WKHxJXasSE4/miyrwfXxxFChn/8vmQv8P
XXNcQ4dfj4JwlNUo7x0cJlXK2E0qRiHsUb8lXwB6X8vW+L6vGQXW0N1Efn/k
xqfOz1pGwXq/54v1t9HzOtl91GlkFFQ4MpUZfdB9p6LD+oPvG9jPbu+N80TP
L1ns+syFbzDtevNCry0OQfUttqxCY8Bc0/VspToOd93VvYVExqB+tb9yE4bu
R8GMSPltY8Cl/4ySSED3gzWxykh+DMJ+pSTt2ovmm5aWfqo5Bmr7Un8Y76Ke
N/HboQtjQGZSyPDcjsNOZ2m3e6VjIHQjP3rTBvR9ZK23zxqLcdBr3tW7F32f
YpfATnw+NgEqIrFBEEIBh8c/d+TMT4BwmJgDlyQFzJ7rbJ+L/w71rJMcGj7d
EE768qNG5wfY163xMY7rggNBEaUO0z9AaYwwm1LUCUFsk5leUT8h0HCFdcvQ
F1hvE2Cz7uAkxMXIp53h+AJKemR14cFJUPvdXdem3gEfVUVF5vx+gfXtPd82
Etshlbm4+rTib+Ac0G+VKfgMfKZT4bLNvyHQSv/TBMNnqIxwlPb3nYJJN2mO
+5ptsMbMp+eE+DQ4bRQj68S1Qv228sTxt9NAHKnQ8Fz4BI0f94hzucxAkmDO
DgGTT3CO+WRtCu8s/LDGKW+qW0BT8AmUls9CROLWuErFFoiS21qQ6jQHIZUb
WmqeNMPN5n16TGvngTmIZ42dTDNkGUXn9b2ahw+j7o4T6U3gfD5URdXsD/Rw
2OwvUGmCZ94v35gu/IGAQpa91R8bQdKFh581YwEUWpjsNps1gtok131xnUV4
/878Vuh8A/j1PJua1F0EvocHL32dbYC6em2Lt8cX4UVa3PPdMw1gmUlUsjq1
CPqrMJXOXw0QYDvRTrJchNW2zdj28QZobquTELqyCEOKhjva8QY4X3K/jCd1
EfY8a59we9cABRnSMj1pi9Bhlm/WUNEAi9GVYbkZiyDjLyUi+7YBQjwW7PRy
FyG/JTB2kNwAhXJOa+6/WoQipnfnLIoagDnpqAlb0yJcWx9ha5nZALrBQ2/b
WhZhr1nh5JuMBoi8cVs2rW0R5nt4msXTG0DapGSFVtcieLht8BtObQC9dTvS
bg0tAtenP/mXSQ0Qw1jNbTC6CLx9V2o6Ehqgd8L62tbxRQiyuiGvHt8AHh/j
dMsnF2FOrvobW0wDkItVCh9OofVU9GpejGoA9vRmEYvZRQgxlRttjmgAgyjn
e/J/FkFFJqJXNbwB4v04fy0uLoKhBaPMo9AG+B/HNrv9
     "]]}, 
   {RGBColor[0.880722, 0.611041, 0.142051], AbsoluteThickness[1.6], Opacity[
    1.], LineBox[CompressedData["
1:eJw91nk01G0bB/AJlS1bJUJNsqWeKHvUJQlRjK3NEkWIJNtTUrYaWtRUZGkb
lGRXiqxjMMYWpkFpJsvMkyGhUsb2zHv/3vec9685n3P/ft9z5v5d13Xfm06e
c/IVwuFwwstwOOxXxKCffPEkAXB47XmJAhaA2PNyZZXU/zvN4cmoaOaz/5sk
dZTfdbkEhtT/5+avSVfbdd+AzNbw/5qxe7RoW+RbIIyZLWAeStnfn1xdAdOd
j5cw+1FmAvd4VgL+nhxOEnnyWw5uCvcOvEIFwphfxS3YWju9g27ZqhWYI9Y5
pz7NeQckVRVxzMZF+YOzM+8gVspOCvOihZA2waoKKPzA1Zi/rNrusBhcBbg+
X3nMlI/HwvPSqgAfWayIOTvnWoYLpQpk3h9UwZwQXFaHG6sCsrPNJsw+JmxO
oWw1mIu8VMdsJSImdmwXctDf2pi1uvS3Lz9VDaXFNTqYxTO9nMtuVgP5VrwB
5gmfWxc8yqtB9+wHU8ydOpWPxdjVMO1bZYG5eJ5DfbO8Brot9tliJjVL87y3
1wD+TpQT5lCS6SqpIzUg8+SsG2YXN7+dVTE1MHRf2xfzuh910XI9NUCSg0uY
+dXjWXVzNaB7qCIR8wBRvuWMai3IfN2VirnG0WJC3q4WcM9ZOZifKAfLNobV
QqxH6WvMsaMZhuce1aL9a2vC7P2q2U2puRbMYy36MavZqOSGydfBtFsubhXy
itUH2jdCHejSmtZh5rHDp9v96mDI85ku5rY88toLJPR8uLcd5oKwjl1q7+og
pErRH/OtPfwT3cN1QHGYIGIOFlO7Fi1eD15dcy8w73x6qYvpVg8UssMPzD/H
frwpT6gH8gUpRSmsPvT9H6UU1INM1eV9mENjvsSHf6gHXEljCGa9NpcAlwWU
Zy6ahTnqxj+ambIUMF4WwcQ8UIJXuqFOAb6JgYQ0cub8g2VnDlGAhDudgHl+
I2PmmDcF6JKGzZiP71/FOxCB1nMUJGSQ15MS3m95jGw7loM56k1dg2IZBci7
rWYxDwzMlYs1o/cf3HeQRX6oHvJwbIICXkIz0nJYvdoW3P6EawB8NfECZveQ
r3GtaxogxExsFLNytbv/S7MG4MX+7luNHD2U5pZBaID05T+OrkFmLf9gf92n
AfLGBcOYHxEOGATcQut1qeLyWH7EVa1j5AZI6m3Ix+z+sF7pQHkD6JbaOa/D
8r8aCG1hNYC/0mCdApYvcf63wnQDaEVkxyti+bqFPFERKvANjQjrkR9fUu3i
baUCz+S0uDLyv2QP6kegAkGiexazJy39Dd2ZCuTI/GkV5A2y0o/yLlFBN+Dc
CjzyFUPbO+l3kA+bq23C+tPtWnxSDhUqK+wdVLF6zF3w92+ngrG6bJcalt9u
6H50EK17NqppYPk/zjvY/KKCTETJdU0s34xnoKXUCArgnKCN/LSHKcQ70wg2
9rb7dyDjZqX/9F9pBJKYkZoespey3VjLvUZUP9/kDZDxfg1dL6oagaKos8sE
q/9bi9S0942A2ycaZIrNqzKjt4kjjaC1dnf5bmTyYtEjP/EmSHp7/oYFlq86
dufIhiZQoPcq7sfyrdUSrHeidQlPqjWWfy8zQPN4E8ho070PIWdpJhqOvmyC
7kDjqWPI41qt1MT6JqjULMrzwOpdW8JBq7cJdGFtojcyddsdvwBcMxw9tqIi
AJsv23t+ick3A9m+TjwY2UlndWz+1mZIim1JDEUe2ZGW/u1wM3wU8hSKRtbW
+6R2K6gZLvzcIxSH9Ye+Utm2+GZQcFXQJSILGz1tPVuE8u4nr7+L9bfZi/kp
YRpQWoIOvUQO2j1GJCnSQGYva7AEuXzP1tU7dGhA0D2T/xbZcm/J1tDjNPDK
tf7ehPWzxY8KuRAa6M64+HcgM/fpWb6+RgOy3iKeiexrVeE+U0oDvjmVwMXm
n/UcL4VGg2lDStsE8h8b0wgDFg1sjskn/UYm2tUnR65sgRDmG8bKQhbkElrq
+B7IFSsZOsiTjmJ2GWEtMFQxTdqFbOhs129yvQV00wOf7kemuXZNRZW3gNat
qDJ35NHj/fglcTqYG0T130bWdVcseoynw1CEq/kj5AsebiZ7DOlA+DOyPB9Z
1GvQMcabDhfkOEU0ZC3f0fhllXSIlW7OEiliQcjpLdLZnXQw1g3Ar0Wu9At8
aMGhw9HKPDENZJszU68TpFrBRmbz5gPIAedmuct9W4G/v+RZCvKrEJPzuVFo
3YCWn4c8f/7SkhWpFXDrxLRrkW+EC9YmVbcCiX/14Shy/sWV1uJybZBnO3TF
opgF/0zUHjHRaAMbd0fuUWS8V7i//642IEw5Us4hP7Aavk472QZamqYjT5Dj
V1d3xL9ug/SWi3NCJSyoJZ5nl7W0AS5FG1SQ+XOak0Of22Bo9M9KY+TgoRRp
EGkHfLnRlnPIbkXBTgsuyGGMPg7yA7z6Ke2AdoilMryFStF5f/9z2LHL7WDc
8SJ6E7JNlE1qxfN2SCfo+3sj61urfgz70w6Vd57u4yFLDvd6TKR1AOmf9aGr
X7HA2uVWsFJhB9Dv2laZIse3WMTYUjoA/2mwwAeZX1RCzuN1QJL+g7uVyNyo
6xwfk06wmUq+7/8anW9rdgewB5BFNzDGy1kwm/jrouRUJ3hls89ueIP6a+Hl
DVPh98B3le9wQs4bli/M2Poe8qTW9tUipxRPT7pGI++YtH38lgVnbZ6Fv1fp
gqQnuaIJleh8TvPeUKzXBSQyfZqOXP51Az35QBeQL8d/kn6Hvt/VDKVDEV1A
oK0qJyMTG25T2zu6wCtclfK+igUOj82SuLu7oTL2MMWjFs0XSyUPpk03+N9r
SSxHDp2Y29Hk3A2S4oxAyToWpJpWfM4J6IZt1+1I9cifP+3QOZXaDaV4qwt6
FHR/ktfsG57oBuOHkurOjWh/apcX9Mx2w7Mw2Yga5Bu+3JgGoR7AB92c12hC
/VaetSVLoQd4MZkFAuQZJ+XLXpY9UBp8obOehubfHVm1wYc9INl6fk90GwvS
RRfPs2wZcLV46NIkgwWbCH8ZZbgygPfe0fbKB1SPaZ6Lrl4MIItt9pdmov3X
aCB2RTCAueHvVINeFgxbEh9SyQwQvSLedbcfzaM4mea8PwzQV0wOvcFC9Ten
phCR/QEIbqHH4kbR/WrvYfaOog9g5vGSaMZjQXdSYvZkxQdQsExRmEMeWTe+
zb/zAxzNMAiKHEf3D8OSvW78DxDUlceO+84CQqhJ4F57JvBPEQ1Yv9Dz44fq
Vs0zwaud6UVexgafwHfNsiK9MHM7W+yiEBtGv6l1rpXqBcnvhZNOwmyYmFj4
rKLaC2anzp0VX86G2cmXc3/Z9kLH83GzJFE2SP5abmCf2QteSoNr66XZYLhQ
W3B7Vx9Yjt1rUdjIhspLW17fs+wDn5qSFgU8G0wXU6oe2PfB1fETAsVNbDBf
Cmx9crIPmGnOa/Cb2WAnUBwtvtEHa+JFO3ZpssFLOFK1a6APtp0y0crXYcN1
ie0Z0pf64aDFG/bZvWzAzSt//3atH3D5EUuFFmyI5EnspZP6gWUVFf19H/p/
zWO82Nx+8NpUlhpqxYY9Mc+Nf/b0Q8eMsGqyHRt+/VT+yNT6CGvusESED7PB
Y0BiXWbfR1DTy8qaCGTDjpdjKWo7ByBJNVQm8SEbsrvlttqYDYDWovyKpUds
kOObNgRaDYB58N9LYU/Y8NPq9vdXxweARxed9MliwyvuTqu9CQNQ6pda5/SC
DTvx0bMezAGwmTjp7vGaDfppUsfTIj+Dz4ucU1odaP+u6W0Ur0Zzc582+yTu
C2w+oV631WgQShdIuLBzX0DMbW5P6cwQ/GzMWBP/4wsUq2t+fWU4AkeJhB2W
IYPALKaq1N/nALMyN1ppcRAmDV+dCBjnQuAKfOE1iyEw/MLjLWz5Cr5Rfup7
44ZAsC9ZKP3qKPyKro4+1DgEf+gXTinTeZDi7hiZ9u8QnJVkuORrjMOg0pH1
46bD8Mk+htB35RsoJ6vQ+eHDEFYadn+8fgKI0lEnnF4MQ3J372lR5UkYt5oz
bR0Yhj7pmDgp7yk4c2y7EX/lCIhr7nH+5DAN4RrGjkU7RiDg+a+/ShenoXZ7
rsQW9xE48frgtoXHP6BvLknreNwIpJJZP9sO/oTpGyvea7wYgf3JD+oC+D/h
/MSXPE7rCCSLzRRFp/+CL67F2Y68EVjrm+S7xmoGyiYfxTmt5IA+gWKO582A
t8zYbqIqBzp3qW5aIP6G3MoSmWJTDuSKVLce1/sDxhqZWf6uHJD3nE3dzvwD
Idovt60K4gDtwRntxPhZuKKQdsMkgQNSJ+JGnNX5cLrE535BGge6NahPp5r4
oO3R86konwOMThN1mdA5cHEUNjap40CgiOv7Z6vnwZzIPZ7YxQHL9S+gjjoP
j94utzs8zIF0nc1vc4MXQGHcaOfUDw4kMC0IwtKL4PI1J64Yx4XioxmvuDWL
0Cbsq/9dmgshQfeNd51YAlNK24s4FS4UxlQ0ev67BIRvASTQ5oJWqNw60YJ/
IV94P/GiERfMZmRuqx8UAGPN5ge2+7hAHCmcnbEXgMp0IVxB7uq28W5yFECl
XcNgGfLJolj9U0cE4LkpRVjRkgtJp6cHyCcFUOi5beArMvNjl6byRQG0VIY3
x1pxIaj2doNcrgDk45/mFh3gwtsC7a0jeQKgkm5qDCELMmgpZQUCiDJxfSxn
y4V7kf/6EcoE4H755rm/kSt1gqVu1wjANaMt19yOCyLZh9zEPghALJx2sucg
F+xJY00fewUwd7nKUfgQF9KuXNue91EAP23KjQyQtd1ql1l/QesSjbx0ZMKa
v/KujgkAF3Vv8IQ9FzKFWmVdJgSgRZsj3kXmTPtc2jwlgBWkKPVG5MjOR/bU
GQEcPMOw1nDgAqXauPLurAB0ThS3H0EWz2du8p4XACWzyOo6skt6yE3dJfR+
MqOyCvkxUfK3QCAAjecbVSeQ/wPrAxy5
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None},
  PlotRange->{{0.001, 2}, {-1.4551915228366852`*^-11, 0.6347561518170195}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{3.706426197362931*^9}]
}, Open  ]]
},
WindowSize->{1302, 725},
WindowMargins->{{-8, Automatic}, {Automatic, -8}},
FrontEndVersion->"10.3 for Microsoft Windows (64-bit) (October 9, 2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 854, 25, 31, "Input"],
Cell[1437, 49, 395, 9, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1869, 63, 308, 10, 31, "Input"],
Cell[2180, 75, 321, 9, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2538, 89, 338, 11, 31, "Input"],
Cell[2879, 102, 315, 9, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3231, 116, 388, 12, 31, "Input"],
Cell[3622, 130, 1104, 34, 60, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4763, 169, 974, 25, 112, "Input"],
Cell[5740, 196, 181, 3, 31, "Output"],
Cell[5924, 201, 200, 4, 31, "Output"],
Cell[6127, 207, 188, 4, 31, "Output"],
Cell[6318, 213, 198, 4, 31, "Output"],
Cell[6519, 219, 179, 3, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6735, 227, 131, 2, 31, "Input"],
Cell[6869, 231, 1019, 31, 60, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7925, 267, 281, 6, 31, "Input"],
Cell[8209, 275, 246, 5, 55, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8492, 285, 240, 6, 31, "Input"],
Cell[8735, 293, 3891, 75, 243, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12663, 373, 193, 5, 31, "Input"],
Cell[12859, 380, 1793, 58, 74, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[14689, 443, 337, 8, 31, "Input"],
Cell[15029, 453, 15824, 272, 236, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

