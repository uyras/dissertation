#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 5.0 patchlevel 4    last modified 2016-07-21 
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2016
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')

set terminal epscairo color size 6.51in,4.98in font "Helvetica,30" lw 1
set output "actualityChart.eps"
set border lw 5
set xlabel "Год" offset 0,0.8
set xtics 2 rotate
set mxtics 1
set ylabel "Суммарное число работ" offset 0.8,0
set ytics 5 offset 0.25,0
set mytics 2
set tics scale 0.5,0.5
set key bottom left Left font "Helvetica,22" reverse at 0.17,0.06 samplen 0.5 spacing 0.75
set style fill solid 1.0

plot [2005.2:2016.8][0:27] \
'data.csv' using 1:2:(0.75) with boxes lc 2 t ''
#    EOF