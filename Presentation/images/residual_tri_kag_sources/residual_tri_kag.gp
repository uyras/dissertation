#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 5.0 patchlevel 4    last modified 2016-07-21 
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2016
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')

set terminal epscairo color size 6.51in,4.98in font "Helvetica,30" lw 1
set output "residual_tri_kag.eps"
set border lw 5
set xlabel "x" 
set xlabel  offset character 8, 1.8
set xtics 0.5 offset 0,0.5
set mxtics 5
set ylabel "S_0/N_{spin}" 
set ylabel  offset character 2.2, 2
set ytics 0.2 offset 0.25,0
set mytics 2
set tics scale 0.5,0.5
set lmargin 6.5
set rmargin 0.5
set tmargin 0.2
set bmargin 1.2
set key top Left left font "Helvetica,22" reverse at 0.05,0.59 samplen 0.5

set arrow 1 from 0.35,-0.07 to 0.35,-0.003 lw 8 lc 20 filled size first 0.03,25
set label "x_c^1=0.35" at 0.16,-0.035 font "Helvetica,25" textcolor lt 20

set arrow 2 from 0.5,0.09 to 0.5,0.02 lw 8 lc 22 filled size first 0.03,25
set label "x_c^2=0.5" at 0.53,0.06 font "Helvetica,25" textcolor lt 22

set arrow from 0.35,0 to 0.35,0.72 nohead lw 8 lc 20 dt 2
set arrow from 0.5,0.09 to 0.5,0.72 nohead lw 8 lc 22 dt 2

plot [-0.02:1.02][0:0.62]\
\
'residual_kagome_l48.dat' using ($1/10):2:3 with yerrorbars lt 5 lc 20 lw 4 ps 0.7 t '', \
'residual_triangular_l48.dat' using ($1/10):2:3 with yerrorbars pt 7 lc 22 lw 4 ps 0.9 t '',\
\
'residual_kagome_l48.dat' using ($1/10):2 lt 5 lc 20 lw 4 ps 0.7 t 'гексагональная	N=3456', \
'residual_triangular_l48.dat' using ($1/10):2 pt 7 lc 22 lw 4 ps 0.9 t 'треугольная 	N=2304',\

#    EOF
