set terminal png size 1000,786 linewidth 2.5 fontscale 2.25
set output 'squareice_24_sr_dos.png'

s=1.03373
m=-6.61882e-7
a=0.702393
ff(x) = (a/(s*sqrt(2*pi))) * exp(-((x-m)**2)/(2*(s**2)))

set style fill transparent solid 1 noborder
set xlabel 'E/(D*N)' offset 0,0.3
set ylabel 'g(E)' offset 1.5,0

plot [-7:7] 'squareice_24_sr.dat' using ($1/24):($2/16777216*2):(0.2) with circles lc 'black' t '',\
ff(x) t ''