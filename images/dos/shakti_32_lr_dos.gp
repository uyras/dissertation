set terminal png size 1000,786 linewidth 2.5 fontscale 2.25
set output 'shakti_32_lr_dos.png'

#set label "c)" at screen 0.02, 0.95

s=0.832612
m=-0.0145062
a=1.8831e-6
ff(x) = (a/(s*sqrt(2*pi))) * exp(-((x-m)**2)/(2*(s**2)))

set style fill transparent solid 1.0 noborder
set xlabel 'E/(D*N)' offset 0,0.3
set ylabel 'g(E)*10^{-5}' offset 1.5,0

plot [-5:5] 'shakti_32_lr.dat' using ($1/32):($2/2147483648.*100000.):(0.05) with circles lc 'black' t '',\
ff(x)*100000 t '' lc 5 lw 2