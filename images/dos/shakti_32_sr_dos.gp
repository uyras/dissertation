set terminal png size 1000,786 linewidth 2.5 fontscale 2.25
set output 'shakti_32_sr_dos.png'

s=0.753442
m=-9.3788e-7
a=0.265462
ff(x) = (a/(s*sqrt(2*pi))) * exp(-((x-m)**2)/(2*(s**2)))

set style fill transparent solid 1 noborder
set xlabel 'E/(D*N)' offset 0,0.3
set ylabel 'g(E)' offset 1.5,0

plot [-5:5] 'shakti_32_sr.dat' using ($1/32):($2/2147483648.):(0.1) with circles lc 'black' t '',\
ff(x) t ''