set terminal png size 1000,786 linewidth 2.5 fontscale 2.25
set output 'squareice_24_lr_dos.png'

#set label "a)" at screen 0.02, 0.95

s=1.14875
m=-0.00718692
a=0.000134101
ff(x) = (a/(s*sqrt(2*pi))) * exp(-((x-m)**2)/(2*(s**2)))

set style fill transparent solid 1.0 noborder
set xlabel 'E/(D*N)' offset 0,0.3
set ylabel 'g(E)*10^{-4}' offset 1.5,0

plot [-7:7] 'squareice_24_lr.dat' using ($1/24):($2/16777216*20000):(0.05) with circles lc 'black' t '',\
ff(x)*10000 t '' lc 5 lw 2