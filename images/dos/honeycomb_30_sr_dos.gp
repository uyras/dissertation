set terminal png size 1000,786 linewidth 2.5 fontscale 2.25
set output 'honeycomb_30_sr_dos.png'

s=0.606506
m=-0.0341316
a=0.176135
ff(x) = (a/(s*sqrt(2*pi))) * exp(-((x-m)**2)/(2*(s**2)))

set style fill transparent solid 1 noborder
set xlabel 'E/(D*N)' offset 0,0.3
set ylabel 'g(E)' offset 1.5,0
#set title 'honeycomb lattice'
plot [-5:5] 'honeycomb_30_sr.dat' using ($1/30):($2/1073741824*2):(0.1) with circles lc 'black' t '',\
ff(x) t ''
