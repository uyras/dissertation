#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 5.0 patchlevel 4    last modified 2016-07-21 
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2016
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')

set terminal epscairo color size 6.51in,4.98in font "Helvetica,30" lw 1
set output "fig5_residual_pyro.eps"
set border lw 5
set xlabel "x" 
set xlabel  offset character 8, 1.8
set xtics 0.5 offset 0,0.5
set mxtics 5
set ylabel "S_0/N_{spin}" 
set ylabel  offset character 2.2, 2
set ytics 0.2 offset 0.25,0
set mytics 2
set tics scale 0.5,0.5
set lmargin 6.5
set rmargin 0.5
set tmargin 0.2
set bmargin 1.2
set key top Left left font "Helvetica,25" reverse at 0.06,0.67

set arrow 1 from 0.61,0.08 to 0.61,0.01 lw 8 lc 7 filled size first 0.03,25
set label "x_c=0.61" at 0.63,0.05 font "Helvetica,25" textcolor lt 7

set arrow from 0.61,0.08 to 0.61,0.72 nohead lw 8 lc 7 dt 2

plot [-0.02:1.02][0:0.72]\
\
'residual_l1.dat' using ($1/16):($2/(16-$1)):($3/(16-$1)) with yerrorbars pt 9 ps 0.8 lc 2 lw 4 t '', \
'residual_l6.dat' using ($1/10):2:3 with yerrorbars lt 7 lc 7 lw 4 ps 0.7 t '', \
\
'residual_l1.dat' using ($1/16):($2/(16-$1)) pt 9 ps 0.8 lc 2 lw 4 t 'L=1, Exact', \
'residual_l6.dat' using ($1/10):2 lt 7 lc 7 lw 4 ps 0.7 t 'L=6, Wang-Landau', \
\
log(2)+log(1/2.0)*3*(1-x)*x*x + log(3/4.0)*2*(1-x)*(1-x)*x + log(3/8.0)*0.5*(1-x)*(1-x)*(1-x) with lines lc black dt 2 lw 3 t 'Pauling'
#    EOF
