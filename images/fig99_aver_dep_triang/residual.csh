#!/bin/sh
DIR="../l48_added"
for x in $(seq 0 9)
do
	RES_FILENAME="residual_0.$x.dat"

	rm -f $RES_FILENAME
	for i in $(seq 1 39)
	do
		for k in $(seq 0 $i)
		do
			tail -n1 $DIR/f_0.${x}_$k.dat >> buff.dat
		done
		awk -f residual.awk $i buff.dat >> $RES_FILENAME
		rm buff.dat
	done
done
