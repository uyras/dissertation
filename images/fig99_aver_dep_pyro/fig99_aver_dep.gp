#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 5.0 patchlevel 4    last modified 2016-07-21 
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2016
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')
set terminal epscairo  transparent enhanced fontscale 1.2 size 7in, 5.5in  linewidth 1
set output "fig99_aver_dep_pyro.eps"
set xlabel "число образцов" 
set xlabel  offset character 0, 0.4
set ylabel "S_0/N_{spin}" 
set ylabel  offset character 3.1, 1.6
set lmargin 7
set rmargin 0.5
set tmargin 0.5
plot [0:41][0.285:0.31] 'residual_0.5.dat' using 4:2:3 with yerrorbars t '' lc black, '' using 4:2 with lines lc black lw 6 t ''
#    EOF
