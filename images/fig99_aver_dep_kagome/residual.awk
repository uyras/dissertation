BEGIN{x=ARGV[1]; ARGV[1]=""}
$4>0 {sum+=$3; sum2+=$3*$3; count++}
END{
    sum = sum/count; sum2=sum2/count-sum*sum;
    printf("%4.2f %9.6f %9.6f %d\n",x,sum,sqrt(sum2/(count-1)),count);
   }
