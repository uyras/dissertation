set terminal pngcairo dashed size 1000,786 linewidth 2.5 fontscale 2.25
set output 'squareice_24_C.png'

set key top left Left reverse
set logscale x
set ylabel "C/(k_BN)" offset 1.6,0
set xlabel 'k_B T/D' offset 0.3,0.5
plot [][-0.01:] 'squareice_24_sr_term.dat' using 1:3 with lines lw 1.5 dt (12,5) lc 'black' t 'близкодействие', \
'squareice_24_lr_term.dat' using 1:3 with lines lw 2 lc 'black' t 'дальнодействие'