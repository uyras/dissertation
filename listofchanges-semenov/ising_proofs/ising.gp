#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 5.0 patchlevel 4    last modified 2016-07-21 
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2016
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')

set terminal epscairo color size 6.51in,4.98in font "Helvetica,23" lw 1
set output "ising_10000.eps"
set border lw 5

set xlabel "k_{B}T/J" font "Helvetica,30"
set xlabel  offset character 0, 0.8
set xtics 0.5 offset 0,0.5
set mxtics 2

set ylabel "C/(k_{B}N)" font "Helvetica,30"
set ylabel  offset character 1.8, -0.5
set ytics 0.5 offset 0.25,0
set mytics 2

set tics scale 0.5,0.5
set lmargin 6.5
set rmargin 0.5
set tmargin 0.2
set bmargin 2.4
set key top left Left font "Helvetica,25" reverse at 0.2,2.55 samplen 0.5

set label "T_c {/Symbol \273} 2.279 J/k_{B}" at 1.75,0.14 font "Helvetica,25" textcolor lt 7

plot [0:4.02][0:2.8]\
'< sort temp_aver.dat' using 1:4 w l lt 8 lc 0 lw 4 ps 0.7 t ''

#    EOF