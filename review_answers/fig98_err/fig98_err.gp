#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 5.0 patchlevel 4    last modified 2016-07-21 
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2016
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')

set terminal epscairo color size 6.51in,4.98in font "Helvetica,30" lw 1
set output "fig98_err.eps"
set border lw 5
set xlabel "x" 
set xlabel  offset character 8, 1.8
set xtics 0.5 offset 0,0.5
set mxtics 5
set ylabel "C_{max}/N_{spin}" 
set ylabel  offset character 2.2, 2
#set ytics 0.1 offset 0.25,0
#set mytics 2
set tics scale 0.5,0.5
set lmargin 6.5
set rmargin 0.5
set tmargin 0.2
set bmargin 1.2
set key bottom left Left font "Helvetica,22" reverse at 0.17,0.06 samplen 0.5 spacing 0.75


plot [-0.02:1.02][0:]\
\
'heat_peaks_kag_small.dat' using ($1/18):3 with points pt 9 ps 1.1 lc 20 lw 4 t 'kagome, N=18, Exact', \
'heat_peaks_kag_l48.dat' using ($1/10):3 with points lt 5 lc 20 lw 4 ps 1.1 t 'kagome, L=48, Wang-Landau', \
'heat_peaks_trian_small.dat' using ($1/16):3 with points pt 7 ps 1.1 lc 22 lw 4 t 'triangular, N=16, Exact', \
'heat_peaks_trian_l48.dat' using ($1/10):3 with points pt 11 lc 22 lw 4 ps 1.1 t 'triangular, L=48, Wang-Landau'

#    EOF