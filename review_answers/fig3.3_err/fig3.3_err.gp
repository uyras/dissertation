#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 5.0 patchlevel 4    last modified 2016-07-21 
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2016
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')

set terminal epscairo color size 7.5in,5in font "Helvetica,30" lw 1
set output "fig33_err.eps"
set border lw 5
set xlabel "k_BT/J" 
set xlabel offset character 8, 1.8
set xtics 2 offset 0,0.5
set mxtics 4
set ylabel "k_BC/N_{spin}" 
set ylabel offset character 2.2, 2
set ytics 0.0004 offset 0.25,0
set mytics 2
set tics scale 0.5,0.5
#set lmargin 6.5
#set rmargin 0.5
set tmargin 0.5
set bmargin 1.5
set key top right font "Helvetica,25"

plot [0:4.95][0:0.00139]\
\
'< sort aver_0.dat' using 1:5 w p lc 7 lw 3 t 'x=0.0', \
'< sort aver_1.dat' using 1:5 w p lc "blue" lw 3 t 'x=0.1', \
'< sort aver_2.dat' using 1:5 w p lc "green" lw 3 t 'x=0.2', \
'< sort aver_3.dat' using 1:5 w p lc 4 lw 3 t 'x=0.3', \
'< sort aver_4.dat' using 1:5 w p lc 0 lw 3 t 'x=0.4', \
'< sort aver_5.dat' using 1:5 w p lc 7 lw 3 t 'x=0.5', \
'< sort aver_6.dat' using 1:5 w p lc "blue" lw 3 t 'x=0.6', \
'< sort aver_7.dat' using 1:5 w p lc "green" lw 3 t 'x=0.7', \
'< sort aver_8.dat' using 1:5 w p lc 4 lw 3 t 'x=0.8', \
'< sort aver_9.dat' using 1:5 w p lc 0 lw 3 t 'x=0.9', \
#    EOF
